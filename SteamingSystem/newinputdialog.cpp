#include "newinputdialog.h"
#include "ui_newinputdialog.h"
#include <QtWidgets>

NewInputDialog::NewInputDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewInputDialog)
{
    ui->setupUi(this);

}

NewInputDialog::~NewInputDialog()
{
    delete ui;
}

void NewInputDialog::on_buttonBox_accepted()
{
    this->hide();
}
