#ifndef OUTPUTSETTINGDIALOG_H
#define OUTPUTSETTINGDIALOG_H

#include <QDialog>

namespace Ui {
class OutputSettingDialog;
}

class OutputSettingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit OutputSettingDialog(QWidget *parent = 0);
    ~OutputSettingDialog();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::OutputSettingDialog *ui;
};

#endif // OUTPUTSETTINGDIALOG_H
