#ifndef HOME_H
#define HOME_H

#include <QMainWindow>
#include <recording.h>
#include <course.h>

namespace Ui {
class Home;
}

class Home : public QMainWindow
{
    Q_OBJECT

public:
    explicit Home(QWidget *parent = 0);
    ~Home();

private slots:
    void on_pbtn_quit_clicked();

    void on_pbtn_recording_clicked();

    void on_pbtn_course_clicked();

private:
    Ui::Home *ui;
    Recording *recording;
    Course *course;
};

#endif // HOME_H
