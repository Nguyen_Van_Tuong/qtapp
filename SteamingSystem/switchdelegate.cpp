#include "switchdelegate.h"
#include "QtWidgets"
#include "QPixmap"
#include "QDebug"

SwitchDelegate :: SwitchDelegate(QObject *parrent, int role){
    type = role;
}

QWidget *SwitchDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{

    QPushButton *switch_btn = new QPushButton(parent);

    switch(type) {
        case 0:
            connect(switch_btn, SIGNAL(clicked(bool)),this,SLOT(on_clicked()));
            break;
        case 1:
            connect(switch_btn, SIGNAL(clicked(bool)),this,SLOT(on_edit_clicked()));
            break;
    }
    return switch_btn;
}

void SwitchDelegate::on_clicked() {
    qInfo("hello");
    QPushButton *check = qobject_cast<QPushButton *>(sender());
    qDebug() << check;
    emit closeEditor(check);
}

void SwitchDelegate::on_edit_clicked(){
    qInfo("OPEN EDIT DIALOG");
    OutputSettingDialog *editDialog = new OutputSettingDialog();
    editDialog->setModal(true);
    editDialog->exec();
}

void SwitchDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const {
    QPushButton *switch_btn = qobject_cast<QPushButton *>(editor);
    QString style;
    if(type == 0) {
        qDebug()<< "setEditorData value =" << index.model()->data(index,Qt::EditRole).toBool();
        bool status = index.model()->data(index).toBool();
        if(status) {
            style = "QPushButton {background-image: url('/home/tuong/Downloads/on.png');background-position: center;background-repeat: none;border: none;background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:0.017, stop:0 rgba(0, 0, 0, 0), stop:1 rgba(255, 255, 255, 0))}QPushButton::pressed{border:none;}";
        } else {
          style = "QPushButton {background-image: url('/home/tuong/Downloads/off.png');background-position: center;background-repeat: none;border: none;background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:0.017, stop:0 rgba(0, 0, 0, 0), stop:1 rgba(255, 255, 255, 0))}QPushButton::pressed{border:none;}";
        }
    }
    else{
        style = "QPushButton {background-image: url('/home/tuong/Downloads/edit_ac.png');background-position: center;background-repeat: none;border: none;background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:0.017, stop:0 rgba(0, 0, 0, 0), stop:1 rgba(255, 255, 255, 0))}QPushButton::pressed{border:none;}";
    }

    switch_btn->setStyleSheet(style);
}

void SwitchDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const {
    if(type == 0) {
        qDebug()<<"setModelData value =" << index.model()->data(index,Qt::EditRole).toBool();
        bool status = index.model()->data(index).toBool();
        QPushButton *switch_btn = qobject_cast<QPushButton *>(editor);
        model->setData(index,!status,Qt::EditRole);
    }
}


