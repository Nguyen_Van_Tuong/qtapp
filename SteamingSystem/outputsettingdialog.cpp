#include "outputsettingdialog.h"
#include "ui_outputsettingdialog.h"

OutputSettingDialog::OutputSettingDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OutputSettingDialog)
{
    ui->setupUi(this);

}

OutputSettingDialog::~OutputSettingDialog()
{
    delete ui;
}

void OutputSettingDialog::on_buttonBox_accepted()
{
    this->hide();
}
