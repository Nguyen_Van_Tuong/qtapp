#include <sliderdelegate.h>
#include <QtWidgets>
#include <QDebug>

SliderDelegate :: SliderDelegate(QObject *parent){}
SliderDelegate :: SliderDelegate( QObject *parent,int mode){
    this->mode = mode;
}
QWidget *SliderDelegate::createEditor(QWidget *parent,
                                     const QStyleOptionViewItem & /* option */,
                                     const QModelIndex &index) const
{
    QPushButton *push_btn = new QPushButton(parent);
    connect(push_btn,SIGNAL(clicked(bool)),this,SLOT(emitClicked()));
    return push_btn;
}
void SliderDelegate::setEditorData(QWidget *editor,const QModelIndex &index) const
{
    QPushButton *push_btn = qobject_cast<QPushButton *>(editor);
    QString uri_on,uri_off;
    if (mode==0) {
        uri_on = "QPushButton{background :rgba(0, 0, 0, 0);background-position: center;border : none;background-image:url(':/icon/on');background-repeat: none;}";
        uri_off = "QPushButton{background :rgba(0, 0, 0, 0);background-position: center;border : none;background-image:url(':/icon/off');background-repeat: none;}";

    }
    else {
//        uri_on = "QPushButton{background :rgba(0, 0, 0, 0);background-position: center;border : none;background-image:url(':/icon/edit');background-repeat: none;}QPushButton:pressed{background :rgba(0, 0, 0, 0);background-position: center;border : none;background-image:url('/home/tuong/Downloads/edit_inac.png');background-repeat: none;}";
//        uri_off = "QPushButton{background :rgba(0, 0, 0, 0);background-position: center;border : none;background-image:url(':/icon/edit');background-repeat: none;}QPushButton:pressed{background :rgba(0, 0, 0, 0);background-position: center;border : none;background-image:url('/home/tuong/Downloads/edit_inac.png');background-repeat: none;}";
//    }
        uri_off ="QPushButton{background :rgba(255, 255, 255, 255)}";
        uri_on = "QPushButton{background :rgba(255, 255, 255, 255)}";
        push_btn->setIcon(QIcon(QPixmap(":/icon/edit")));
        push_btn->setIconSize(QSize(20,20));
        push_btn->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);


        }

    if(index.model()->data(index).toString().compare(" "))
        push_btn->setStyleSheet(uri_on);
    else
        push_btn->setStyleSheet(uri_off);
}

void SliderDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,const QModelIndex &index) const
{
       if(index.model()->data(index).toString().compare(" "))

    {
        qDebug()<<(index.model()->data(index).toString())<<"------true";
        model->setData(index," ",Qt::DisplayRole);
        setEditorData(editor,index);

    }
    else
    {
        qDebug()<<index.model()->data(index).toString()<<"------flase";
        model->setData(index,"",Qt::DisplayRole);
        setEditorData(editor,index);

    }

}

void SliderDelegate::emitClicked()
{
emit closeEditor(qobject_cast<QPushButton *>(sender()));

    emit closeEditor(qobject_cast<QPushButton *>(sender()));

}
