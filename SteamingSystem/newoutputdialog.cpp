#include "newoutputdialog.h"
#include "ui_newoutputdialog.h"
#include "QLayout"
#include "QDebug"
#include <outputsettingdialog.h>
#include <sliderdelegate.h>

NewOutputDialog::NewOutputDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewOutputDialog)
{
    ui->setupUi(this);
    QStringList _header;
    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setRowCount(4);
    _header << "Current Input" << "Action" << "";
    ui->tableWidget->setHorizontalHeaderLabels(_header);
    ui->tableWidget->setColumnWidth(0,300);
    ui->tableWidget->setColumnWidth(1,60);
    ui->tableWidget->setColumnWidth(2,150);
    ui->tableWidget->setItemDelegateForColumn(1, new SliderDelegate(parent,0));
    ui->tableWidget->setItemDelegateForColumn(2, new SliderDelegate(parent,1));

    QTableWidgetItem *item0 = new QTableWidgetItem("Monitor");
    item0->setFlags(item0->flags() & ~(Qt::ItemIsEditable && Qt::ItemIsSelectable));
    ui->tableWidget->setItem(0, 0, item0);
    QTableWidgetItem *item1 = new QTableWidgetItem("");
    item1->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEditable);
    ui->tableWidget->setItem(0, 1, item1);
    ui->tableWidget->openPersistentEditor((item1));
    QTableWidgetItem *item2 = new QTableWidgetItem();
    item2->setFlags(~(Qt::ItemIsSelectable | Qt::ItemIsEditable));
    ui->tableWidget->openPersistentEditor((item2));
    QTableWidgetItem *item3 = new QTableWidgetItem(" ");
    item3->setFlags(item3->flags() & ~Qt::ItemIsEditable);
    ui->tableWidget->setItem(0, 2, item3);
    ui->tableWidget->openPersistentEditor((item3));
    connect(ui->rbtn_hdd,SIGNAL(clicked(bool)),this,SLOT(on_pbtn_hdd_clicked()));
    connect(ui->rbtn_live,SIGNAL(clicked(bool)),this,SLOT(on_pbtn_live_clicked()));

}

NewOutputDialog::~NewOutputDialog()
{
    delete ui;
}
void NewOutputDialog::setOutputName(QString str){
    ui->output_name->setText(str);
}

void NewOutputDialog::on_qbtn_add_input_clicked()
{
    qInfo("ADD NEW INPUT");
}

void NewOutputDialog::on_pbtn_hdd_clicked()
{
    ui->groupBox->setTitle("HDD Configuration");
    ui->config_name->setText("File");
    ui->use_config->setText("mp4");

}

void NewOutputDialog::on_pbtn_live_clicked()
{
    ui->groupBox->setTitle("LIVE Configuration");
    ui->config_name->setText("RTMP url ");
    ui->use_config->setText("rtmp://abc.xyz");

}

void NewOutputDialog::on_rbtn_live_clicked()
{

}

void NewOutputDialog::on_bnt_adv_clicked()
{
    OutputSettingDialog *outputSettingDialog = new OutputSettingDialog(this);
    outputSettingDialog->setWindowTitle("Advance Setting");
    outputSettingDialog->setModal(false);
    outputSettingDialog->exec();
}


void NewOutputDialog::on_pushButton_clicked()
{
    inputDialog = new NewInputDialog(this);
    inputDialog->setModal(true);
    inputDialog->exec();
}
