#ifndef SWITCHDELEGATE_H
#define SWITCHDELEGATE_H

#include "QItemDelegate"
#include "QPushButton"
#include "outputsettingdialog.h"

class SwitchDelegate : public QItemDelegate {
    Q_OBJECT

public:
    explicit SwitchDelegate(QObject *parrent = 0, int role = 0);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor,const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

public slots:
    void on_clicked();
    void on_edit_clicked();

private:
    int type;
};



#endif // SWITCHDELEGATE_H
