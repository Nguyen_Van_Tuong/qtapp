#ifndef NEWOUTPUTDIALOG_H
#define NEWOUTPUTDIALOG_H

#include <QDialog>
#include "newinputdialog.h"

namespace Ui {
class NewOutputDialog;
}

class NewOutputDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewOutputDialog(QWidget *parent = 0);
    ~NewOutputDialog();
public:
    void setOutputName(QString str);
private slots:
    void on_qbtn_add_input_clicked();

    void on_pbtn_hdd_clicked();

    void on_pbtn_live_clicked();

    void on_rbtn_live_clicked();

    void on_bnt_adv_clicked();

    void on_pushButton_clicked();

private:
    Ui::NewOutputDialog *ui;
    NewInputDialog *inputDialog;
};

#endif // NEWOUTPUTDIALOG_H
