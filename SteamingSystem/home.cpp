#include "home.h"
#include "ui_home.h"

Home::Home(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Home)
{
    ui->setupUi(this);
}

Home::~Home()
{
    delete ui;
}

void Home::on_pbtn_quit_clicked()
{
    qInfo("Quit");
    this->close();
}

void Home::on_pbtn_recording_clicked()
{
    this->recording = new Recording();
    this->recording->show();
    this->hide();
}

void Home::on_pbtn_course_clicked()
{
    course = new Course();
    course->show();
    this->hide();
}
