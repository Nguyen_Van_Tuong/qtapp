#ifndef RECORDING_H
#define RECORDING_H

#include <QWidget>
#include "newoutputdialog.h"
#include "QListWidgetItem"
#include<QTableWidgetItem>

namespace Ui {
class Recording;
}

class Recording : public QWidget
{
    Q_OBJECT

public:
    explicit Recording(QWidget *parent = 0);
    ~Recording();

private slots:
    void on_qbtn_add_output_clicked();

    void on_qbtn_back_clicked();

    void changeItem(QTableWidgetItem *item );

    void on_start_btn_clicked();

    void on_pause_btn_clicked();

    void on_stop_btn_clicked();

private:
    Ui::Recording *ui;
};

#endif // RECORDING_H
