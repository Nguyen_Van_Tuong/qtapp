#include "recording.h"
#include "ui_recording.h"
#include <home.h>
#include "QDebug"
#include "switchdelegate.h"
#include <sliderdelegate.h>

Recording::Recording(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Recording)
{
   ui->setupUi(this);
   QStringList _header;
   ui->output_tb->setColumnCount(3);
   ui->output_tb->setRowCount(4);
   _header << "Current Output" << "Action" << "";
   ui->output_tb->setHorizontalHeaderLabels(_header);
   ui->output_tb->setColumnWidth(0,300);
   ui->output_tb->setColumnWidth(1,60);
   ui->output_tb->setColumnWidth(2,180);
   ui->output_tb->setItemDelegateForColumn(1, new SliderDelegate(parent,0));
   ui->output_tb->setItemDelegateForColumn(2, new SliderDelegate(parent,1));

   QTableWidgetItem *item0 = new QTableWidgetItem("303B9 Live");
   item0->setFlags(item0->flags() & ~(Qt::ItemIsEditable && Qt::ItemIsSelectable));
   ui->output_tb->setItem(0, 0, item0);
   QTableWidgetItem *item1 = new QTableWidgetItem("");
   item1->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEditable);
   ui->output_tb->setItem(0, 1, item1);
   ui->output_tb->openPersistentEditor((item1));
   QTableWidgetItem *item2 = new QTableWidgetItem();
   item2->setFlags(~(Qt::ItemIsSelectable | Qt::ItemIsEditable));
   ui->output_tb->openPersistentEditor((item2));
   QTableWidgetItem *item3 = new QTableWidgetItem(" ");
   item3->setFlags(item3->flags() & ~Qt::ItemIsEditable);
   ui->output_tb->setItem(0, 2, item3);
   ui->output_tb->openPersistentEditor((item3));

   ui->pause_btn->setEnabled(false);

   connect(ui->output_tb,SIGNAL(itemChanged(QTableWidgetItem*)),this,SLOT(changeItem(QTableWidgetItem*)));
   connect(ui->btn_add,SIGNAL(clicked()),this,SLOT(on_qbtn_add_output_clicked()));

}

Recording::~Recording()
{
    delete ui;

}

void Recording::on_qbtn_add_output_clicked()
{
    qInfo("ADD NEW OUTPUT");
    NewOutputDialog *new_output = new NewOutputDialog(this);
    new_output->setWindowTitle("New Output");
    new_output->setModal(false);
    new_output->exec();

}

void Recording::on_qbtn_back_clicked()
{
    Home *homepage = new Home();
    homepage->show();
    this->hide();
}
void Recording::changeItem(QTableWidgetItem *item)
{
    if(item->column()==2){
        NewOutputDialog *editOutputDialog = new NewOutputDialog(this);
        editOutputDialog->setModal(false);
        editOutputDialog->setOutputName(ui->output_tb->itemAt(item->row(),0)->data(0).toString());
        editOutputDialog->exec();
    }
}



void Recording::on_start_btn_clicked()
{
    ui->pause_btn->setEnabled(true);
    ui->start_btn->setEnabled(false);
}

void Recording::on_pause_btn_clicked()
{
    ui->pause_btn->setEnabled(false);
    ui->start_btn->setEnabled(true);
}

void Recording::on_stop_btn_clicked()
{
    ui->pause_btn->setEnabled(false);
    ui->start_btn->setEnabled(true);
}
