#-------------------------------------------------
#
# Project created by QtCreator 2017-11-27T17:38:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SteamingSystem
TEMPLATE = app


SOURCES += main.cpp\
        home.cpp \
    recording.cpp \
    course.cpp \
    newoutputdialog.cpp \
    outputsettingdialog.cpp \
    switchdelegate.cpp \
    sliderdelegate.cpp \
    newinputdialog.cpp

HEADERS  += home.h \
    recording.h \
    course.h \
    newoutputdialog.h \
    outputsettingdialog.h \
    switchdelegate.h \
    sliderdelegate.h \
    newinputdialog.h

FORMS    += home.ui \
    recording.ui \
    course.ui \
    newoutputdialog.ui \
    outputsettingdialog.ui \
    newinputdialog.ui

DISTFILES +=

RESOURCES += \
    image.qrc
