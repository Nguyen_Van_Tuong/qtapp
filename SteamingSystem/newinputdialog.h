#ifndef NEWINPUTDIALOG_H
#define NEWINPUTDIALOG_H

#include <QDialog>

namespace Ui {
class NewInputDialog;
}

class NewInputDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewInputDialog(QWidget *parent = 0);
    ~NewInputDialog();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::NewInputDialog *ui;
};

#endif // NEWINPUTDIALOG_H
