#include "course.h"
#include "ui_course.h"
#include "home.h"

Course::Course(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Course)
{
    ui->setupUi(this);
}

Course::~Course()
{
    delete ui;
}

void Course::on_pbtn_save_clicked()
{
    Home *homepage = new Home();
    homepage->show();
    this->hide();
}
