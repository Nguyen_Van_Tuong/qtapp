#ifndef SLIDERDELEGATE_H
#define SLIDERDELEGATE_H
#include <QItemDelegate>

class SliderDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit SliderDelegate(QObject *parent = 0);
    explicit SliderDelegate(QObject *parent = 0,int mode = 0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const;

signals:

private:
    int mode = 0;
public slots:
        void emitClicked();

};


#endif // SLIDERDELEGATE_H
